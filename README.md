# EngageBay

Module provides the flexibility of publishing the landing pages created
from EngageBay into drupal pages.
The Module also provides the flexibility of creating the forms created in
EngageBay wherever it is required in drupal.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/engagebay).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/engagebay).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Maintainers

- EngageBay - [engagebay](https://www.drupal.org/u/engagebay)
