(function ($, DRUPAL, CKEDITOR) {
  'use strict';

  var insertContent;
  var formCallback = function (data) {
    let content = data.html_render;
    insertContent(content);
  };

  CKEDITOR.plugins.add('form', {
    init: function (editor) {
      editor.addCommand('form', {
        modes: {
          wysiwyg: 1
        },
        canUndo: true,
        exec: function (editor, data) {
          // Set existing values for future updates.
          let existingValues = {};

          let dialogSettings = {
            title: editor.config.engagebay_dialogFormTitle,
            dialogClass: 'editor-form-dialog'
          };
          Drupal.ckeditor.openDialog(editor, Drupal.url("engagebay/dialog/form/".concat(editor.config.drupal.format)), existingValues, formCallback, dialogSettings);
        }
      });

      editor.ui.addButton('Form', {
        label: Drupal.t('EngageBay Forms'),
        command: 'form',
        icon: this.path + 'icons/formicon.png'
      })

      insertContent = function (html) {
        editor.insertHtml(html);
      }
    },
  })

})(jQuery, Drupal, CKEDITOR);
