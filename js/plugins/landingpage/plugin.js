(function ($, DRUPAL, CKEDITOR) {
  'use strict';

  var insertContent;
  var landingPageCallback = function (data) {
    let content = data.html_render;
    insertContent(content);
  };

  CKEDITOR.plugins.add('landingpage', {
    init: function (editor) {
      editor.addCommand('landingpage', {
        modes: {
          wysiwyg: 1
        },
        canUndo: true,
        exec: function (editor, data) {
          // Set existing values for future updates.
          let existingValues = {};

          let dialogSettings = {
            title: editor.config.engagebay_dialogLandingPageTitle,
            dialogClass: 'editor-landing-page-dialog'
          };
          Drupal.ckeditor.openDialog(editor, Drupal.url("engagebay/dialog/landingpage/".concat(editor.config.drupal.format)), existingValues, landingPageCallback, dialogSettings);
        }
      });

      editor.ui.addButton('LandingPage', {
        label: Drupal.t('EngageBay Landing Page'),
        command: 'landingpage',
        icon: this.path + 'icons/landingpageicon.png'
      })

      insertContent = function (html) {
        editor.insertHtml(html);
      }
    },
  })

})(jQuery, Drupal, CKEDITOR);
