<?php

namespace Drupal\engagebay\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\engagebay\Service\EngageBayAPI;
use Drupal\filter\Entity\FilterFormat;

/**
 * Class for creating From Dialog.
 */
class EngageBayFormDialog extends FormBase implements BaseFormIdInterface {

  /**
   * Returns the formid.
   */
  public function getFormId() {
    return 'editor_engagebay_form__dialog';
  }

  /**
   * Returns base form id.
   */
  public function getBaseFormId() {
    // Use the EditorEngagebayLandingPageDialog form id to ease alteration.
    return 'editor_engagebay_form_dialog';
  }

  /**
   * Built the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, FilterFormat $filter_format = NULL) {
    // The default values are set directly from \Drupal::request()->request,
    // provided by the editor plugin opening the dialog.
    // $user_input = $form_state->getUserInput();
    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="editor-form-dialog-form">';
    $form['#suffix'] = '</div>';

    $form['engagebay_form_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Forms'),
      '#options' => $this->getOptionsForForms(),
      '#default_value' => '',
      '#attributes' => [
        'style' => 'width: 300px',
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * Function to submit form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -1,
      ];
      $response->addCommand(new HtmlCommand('#editor-form-dialog-form', $form));
    }
    else {

      $engagebay_form_id = $form_state->getValue('engagebay_form_id');
      $engagebayAPI = new EngageBayAPI($this->configFactory()->get('engagebay.settings')->get('rest_api_key'));
      $engagebay_form_response = $engagebayAPI->getForms();
      if (count($engagebay_form_response) >= 1) {
        $form_state->setValue('html_render', "{engagebayform_id}" . $engagebay_form_id . "{/engagebayform_id}");
      }

      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

  /**
   * Returns the options form from to select.
   */
  private function getOptionsForForms() {
    $engagebay_form_options_for_select = [];

    $engagebay_config = $this->configFactory()->get('engagebay.settings');
    if ($engagebay_config && $engagebay_config->get('rest_api_key')) {
      $engagebayAPI = new EngageBayAPI($engagebay_config->get('rest_api_key'));

      $engagebay_forms = $engagebayAPI->getForms();
      if (count($engagebay_forms) > 0) {
        foreach ($engagebay_forms as $engagebay_form) {
          $engagebay_form_options_for_select[$engagebay_form['shareLinkId']] = $engagebay_form['name'];
        }
      }
    }

    return $engagebay_form_options_for_select;
  }

}
