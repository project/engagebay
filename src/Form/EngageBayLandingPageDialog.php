<?php

namespace Drupal\engagebay\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\engagebay\Service\EngageBayAPI;
use Drupal\filter\Entity\FilterFormat;

/**
 * Class to show dialog on landing page.
 */
class EngageBayLandingPageDialog extends FormBase implements BaseFormIdInterface {

  /**
   * Get the form id.
   */
  public function getFormId() {
    return 'editor_engagebay_landing_page__dialog';
  }

  /**
   * Get the base form id.
   */
  public function getBaseFormId() {
    // Use the EditorEngagebayLandingPageDialog form id to ease alteration.
    return 'editor_engagebay_landing_page_dialog';
  }

  /**
   * Function to build the Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, FilterFormat $filter_format = NULL) {
    // The default values are set directly from \Drupal::request()->request,
    // provided by the editor plugin opening the dialog.
    // $user_input = $form_state->getUserInput();
    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="editor-landing-page-dialog-form">';
    $form['#suffix'] = '</div>';

    $form['landing_page_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Landing Page'),
      '#options' => $this->getOptionsForLandingPages(),
      '#default_value' => '',
      '#attributes' => [
        'style' => 'width: 300px',
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * Function to submit the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -1,
      ];
      $response->addCommand(new HtmlCommand('#editor-landing-page-dialog-form', $form));
    }
    else {

      $landing_page_id = $form_state->getValue('landing_page_id');
      $engagebayAPI = new EngageBayAPI($this->configFactory()->get('engagebay.settings')->get('rest_api_key'));
      $landing_page_response = $engagebayAPI->getLandingPage($landing_page_id);
      if (count($landing_page_response) >= 1) {
        $form_state->setValue('html_render', "{engagebaylandingpage_id}" . $landing_page_id . "{/engagebaylandingpage_id}");
      }

      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

  /**
   * Returns the options for landing page Dialog.
   */
  private function getOptionsForLandingPages() {
    $landing_page_options_for_select = [];

    $engagebay_config = $this->configFactory()->get('engagebay.settings');
    if ($engagebay_config && $engagebay_config->get('rest_api_key')) {
      $engagebayAPI = new EngageBayAPI($engagebay_config->get('rest_api_key'));

      $landing_pages = $engagebayAPI->getLandingPages();
      if (count($landing_pages) > 0) {
        foreach ($landing_pages as $landing_page) {
          $landing_page_options_for_select[$landing_page['shareLinkId']] = $landing_page['name'];
        }
      }
    }

    return $landing_page_options_for_select;
  }

}
