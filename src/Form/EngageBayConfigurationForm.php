<?php

namespace Drupal\engagebay\Form;

use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\engagebay\Service\EngageBayAPI;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for creating configuration form.
 */
class EngageBayConfigurationForm extends FormBase {
  const ENGAGEBAY_VARS = 'engagebay_vars';

  /**
   * Get the formid.
   */
  public function getFormId(): string {
    return 'engagebay_configuration_form';
  }

  /**
   * Build the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $engagebay_domain = $this->configFactory()->getEditable('engagebay.settings')->get('domain');

    if (array_key_exists('HTTP_REFERER', $_SERVER)) {
      if (!str_contains($_SERVER['HTTP_REFERER'], 'engagebay/home') && $engagebay_domain) {
        return $this->redirect('engagebay.home');
      }
    }
    elseif ($engagebay_domain) {
      return $this->redirect('engagebay.home');
    }

    $this->configFactory()->getEditable('engagebay.settings')->delete();

    $form['username'] = [
      '#type' => 'email',
      '#title' => $this->t('Username:'),
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password:'),
      '#required' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Connect'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Function to submit the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->isValidationComplete() && array_key_exists(self::ENGAGEBAY_VARS, $form)) {
      $this->configFactory()
        ->getEditable('engagebay.settings')
        ->set('domain', $form[self::ENGAGEBAY_VARS]['domain_name'])
        ->set('email', $form[self::ENGAGEBAY_VARS]['email'])
        ->set('rest_api_key', $form[self::ENGAGEBAY_VARS]['api_key']['rest_API_Key'])
        ->set('js_api_key', $form[self::ENGAGEBAY_VARS]['api_key']['js_API_Key'])
        ->save();

      $form_state->setRedirectUrl(Url::fromRoute('engagebay.home'));
    }
  }

  /**
   * Function to validate the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $engageBay = new EngageBayAPI();
    $login_response = $engageBay->login($form_state->getValue('username'), $form_state->getValue('password'));

    if ($login_response) {
      $form[self::ENGAGEBAY_VARS] = $login_response;
    }
    else {
      $form_state->setErrorByName('Login', 'Invalid Credentials or Too many login attempts');
      $this->configFactory()
        ->getEditable('engagebay.settings')
        ->delete();
    }
  }

  /**
   * Change the response on input.
   */
  public function onInputChange(array &$form, FormStateInterface $form_state, Request $request) {
    $response = new AjaxResponse();

    $response->addCommand(new InvokeCommand('submit-config', 'onInputChange', ['Connect']));

    return $response;
  }

}
