<?php

namespace Drupal\engagebay\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Form" plugin.
 *
 * @CKEditorPlugin(
 *   id = "form",
 *   label = @Translation("Forms"),
 *   module = "engagebay"
 * )
 */
class Form extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

  /**
   * Function to get the file.
   */
  public function getFile() {
    return $this->getModulePath('engagebay') . '/js/plugins/form/plugin.js';
  }

  /**
   * Gets the library.
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/drupal.ajax',
    ];
  }

  /**
   * Returns the configuration.
   */
  public function getConfig(Editor $editor) {
    return [
      'engagebay_dialogFormTitle' => $this->t('Form'),
    ];
  }

  /**
   * Returns the Form Buttons.
   */
  public function getButtons() {
    $iconImage = $this->getModulePath('engagebay') . '/js/plugins/form/icons/formicon.png';

    return [
      'Form' => [
        'label' => $this->t('Forms'),
        'image' => $iconImage,
      ],
    ];
  }

  /**
   * Return the Form Settings.
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    $form_state->loadInclude('editor', 'admin.inc');

    return $form;

  }

}
