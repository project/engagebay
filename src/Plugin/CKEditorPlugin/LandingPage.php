<?php

namespace Drupal\engagebay\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Landing Page" plugin.
 *
 * @CKEditorPlugin(
 *   id = "landingpage",
 *   label = @Translation("Landing Page"),
 *   module = "engagebay"
 * )
 */
class LandingPage extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

  /**
   * Get the File of landing page.
   */
  public function getFile() {
    return $this->getModulePath('engagebay') . '/js/plugins/landingpage/plugin.js';
  }

  /**
   * Returns the library path of the landing page.
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/drupal.ajax',
    ];
  }

  /**
   * Returns to get config.
   */
  public function getConfig(Editor $editor) {
    return [
      'engagebay_dialogLandingPageTitle' => $this->t('Landing Page'),
    ];
  }

  /**
   * Function to returns the buttons on landing page.
   */
  public function getButtons() {
    $iconImage = $this->getModulePath('engagebay') . '/js/plugins/landingpage/icons/landingpageicon.png';

    return [
      'LandingPage' => [
        'label' => $this->t('Landing Pages'),
        'image' => $iconImage,
      ],
    ];
  }

  /**
   * Returns the forom settings.
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    $form_state->loadInclude('editor', 'admin.inc');

    return $form;

  }

}
