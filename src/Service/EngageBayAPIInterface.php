<?php

namespace Drupal\engagebay\Service;

/**
 * Define values of API parameters.
 */
interface EngageBayAPIInterface {
  public const BASE_API_URI = 'https://app.engagebay.com/dev/api/panel/';
  public const DOMAIN_LOGIN = 'https://app.engagebay.com/rest/api/login/get-domain';
  public const GET_LANDING_PAGES = self::BASE_API_URI . 'landingPage';
  public const GET_FORMS = self::BASE_API_URI . 'forms';

}
