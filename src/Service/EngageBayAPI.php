<?php

namespace Drupal\engagebay\Service;

use GuzzleHttp\Client;

/**
 * Handle API requests using API interface.
 */
class EngageBayAPI implements EngageBayAPIInterface {
  private const CONTENT_TYPE_FORM = 'application/x-www-form-urlencoded';
  private const CONTENT_TYPE_JSON = 'application/json';

  private ?Client $client;

  private array $headers = [
    'accept' => self::CONTENT_TYPE_JSON,
    'content-type' => 'application/json',
  ];

  /**
   * Constructs a EngageBayAPI object.
   */
  public function __construct(string $restApiKey = NULL) {
    $this->client = \Drupal::httpClient();

    if ($restApiKey) {
      $this->headers['Authorization'] = $restApiKey;
    }
  }

  /**
   * Returns if User logged on resultant of POST request.
   */
  public function login(string $username, string $password): ?array {
    $this->headers['content-type'] = 'application/x-www-form-urlencoded';
    $this->headers['accept'] = '*/*';

    $body = [
      'email' => $username,
      'password' => $password,
      'source' => 'WORDPRESS',
    ];

    return $this->post(self::DOMAIN_LOGIN, $body, FALSE);
  }

  /**
   * Function to returns landing pages.
   */
  public function getLandingPages(): ?array {
    return $this->get(self::GET_LANDING_PAGES);
  }

  /**
   * Returns all the forms.
   */
  public function getForms(): ?array {
    return $this->get(self::GET_FORMS);
  }

  /**
   * Function to returns landing pages.
   */
  public function getLandingPage(string $landing_page_id): ?array {
    return $this->get(sprintf(self::GET_LANDING_PAGES, $landing_page_id));
  }

  /**
   * Make POST request.
   */
  public function post(string $uri, array $data): ?array {
    return $this->sendRequest('POST', $uri, $data);
  }

  /**
   * Make GET request.
   */
  public function get(string $uri): ?array {
    return $this->sendRequest('GET', $uri, []);
  }

  /**
   * Fucnction to send Request.
   */
  public function sendRequest(string $type, string $uri, array $data) {
    try {
      $response = $this->client->request($type, $uri, [
        'headers' => $this->headers,
        'form_params' => $data,
      ]);

      $response_body = $response->getBody();
      if ($response->getStatusCode() === 200) {
        return $this->isJson($response_body) ? json_decode($response_body, JSON_PRETTY_PRINT) : $response_body;
      }

    }
    catch (\Exception $e) {
      \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(
        \Drupal::VERSION, '10.1.0',
        fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('engagebay'), $e, $e->getMessage()),
        fn() => watchdog_exception('engagebay', $e, $e->getMessage())
      );
    }

    return NULL;
  }

  /**
   * Returns true if the given data is json encoded.
   */
  private function isJson(string $str): bool {
    json_decode($str);

    return json_last_error() === JSON_ERROR_NONE;
  }

}
